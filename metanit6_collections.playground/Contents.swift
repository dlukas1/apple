import UIKit

//Последовательность (range) представляет набор значений, который определяется начальной и конечной точкой.
let range = 1...5
let range2 = 3...8

print(range)

for num in range.reversed(){
    print(num)
}

//Метод overlaps возвращает true, если две последовательности хотя бы частично совпадают:
print(range.overlaps(range2))


//Массивы
var numbers: [Int] = [1,2,3,4,5,4,3,2,1]
//Создание массива из последовательности
var sequenceArray = Array(5...15)
//Еще одна форма инициализатора позволяет инициализировать массив определенным числом элементов одного значения:
var Zzz = [Character](repeating: "z", count: 5)
print(Zzz)

//Добавление в массив
numbers.append(22)
numbers.insert(33, at: 3)
print(numbers)

//Удаление из массива
/*  Ряд операций позволяют удалять элемент:
 remove(at: index): удаляет элемент по определенному индексу
 removeFirst(): удаляет первый элемент
 removeLast(): удаляет последний элемент
 dropFirst(): удаляет первый элемент
 dropLast(): удаляет последний элемент
 removeAll(): удаляет все элементы массива*/


//Для сортировки массива применяется метод sort
var unsorted = [78,43,45,77,12,34]
unsorted.sort()
print(unsorted)


//Объединение массивов
var joined = numbers + unsorted
print(joined)

//Фильтрация массивов
var oddNums = joined.filter({$0%2==1})
print("Odd numbers are \(oddNums)")
//Еще один способ фильтрации представляет метод prefix(). Он также возвращает отфильтрованный массив, при этом он перебирает подряд все элементы, пока условие возвращает true.
var smallerThan30 = joined.prefix(while: {$0<30})
print(smallerThan30)

//Преобразование массива
var simpleNums = [3,4,5,6,7,8]
var mappedNums = simpleNums.map({$0 * $0})
print(mappedNums)
