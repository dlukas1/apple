import UIKit


//CLOSURES
let sum = {
    (x: Int, y:Int) -> Int in
        return x+y
}
print(sum(3,4))


func action() -> (()->Int){
    
    var val = 0
    return {
        val = val+5
        return val
    }
}
let inc = action()
print(inc())    // 1
print(inc())    // 2


//OVERRIDE
/*
func sum (_ a: Int, _ b: Int) -> Int{
    return a + b
}
func sum(_ a:String, _ b : String)->String{
    return a + b
}

print(sum(3,4))
print(sum("Three", "Four"))

*/
