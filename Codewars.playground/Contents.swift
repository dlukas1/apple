import UIKit
/*Complete the function that takes two numbers as input, num and nth and return the nth digit of num (counting from right to left).
 
 Note
 If num is negative, ignore its sign and treat it as a positive value
 If nth is not positive, return -1
 Keep in mind that 42 = 00042. This means that findDigit(42, 5) would return 0
 findDigit(5673, 4)     returns 5
 findDigit(129, 2)      returns 2
*/

func findDigit(_ num: Int, _ nth: Int) -> Int{
    if nth <= 0{
        return -1
    }
    return(abs(num) / Int(pow(10, Double(nth - 1))))%10
}

findDigit(12345, 3)
