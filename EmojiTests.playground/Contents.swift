import UIKit


func  GetEmojis() -> [String] {
    //emojiList is array of arrays with different sets of emoji
    var emojiList = [
        ["🎃","👻","🤡","💩","☠️","👹","🤖","👺"],
        ["🐼","🐶","🐱","🐭","🐹","🐰","🦊","🐻"],
        ["🚨","🚀","🛰","⛵️","🏎","🚲","🚜","🚌"],
        ["🏳️","🏳️‍🌈","🏁","🚩","🇦🇶","🇪🇪","🇧🇳","🇨🇦"]
    ]
    let index = Int(arc4random_uniform(UInt32(emojiList.count)))
    return emojiList[index]
}

print(GetEmojis())
for i in 0...10{
    print(GetEmojis())
}
