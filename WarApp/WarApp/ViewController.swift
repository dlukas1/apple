//
//  ViewController.swift
//  WarApp
//
//  Created by Dmitry Lukas on 10/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var leftScoreLabel: UILabel!
    @IBOutlet weak var rightScoreLabel: UILabel!
    
    var playerScore = 0
    var cpuScore = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func dealTapped(_ sender: Any) {

        //arc4random_uniform gives random to upper bound excluded
        //but no lower bound
        let leftRandomNr = 2 + arc4random_uniform(13)
        let rightRandomNr = 2 + arc4random_uniform(13)
        
        //UIImage(named: "name of assets file")
        leftImageView.image = UIImage(named: "card\(leftRandomNr)")
        rightImageView.image = UIImage(named: "card\(rightRandomNr)")
        
        
        
        if leftRandomNr > rightRandomNr{
            playerScore +=  1
            //update label
            leftScoreLabel.text = String(playerScore)
        }
        else if rightRandomNr > leftRandomNr{
            cpuScore += 1
            //update label
            rightScoreLabel.text = String(cpuScore)
        }
        else{
            //no actions needed here
        }
    }
}

