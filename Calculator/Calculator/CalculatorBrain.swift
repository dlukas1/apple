import Foundation

struct CalculatorBrain {
    
    private var accumulator : Double?
    
    var result : Double?{
        get {return accumulator}
    }
    
    mutating func performOperation(_ symbol : String){
        switch symbol{
        case "√":
            if let operand = accumulator{
                    accumulator = sqrt(operand)
            }
        case "^2":
            if let operand = accumulator{
                accumulator = operand * operand
            }
        case "!":
            accumulator = calculateFactorial(Int(accumulator!) )
        
        case "del":
            accumulator = 0
 
        default:
            break
        }
    }
    
    mutating func setOperand(_ operand : Double){
        accumulator = operand
    }
    
    func calculateFactorial(_ num : Int) -> Double{
        if num == 0{
            return 1
        }
        var a : Double = 1
        for i in 1...num{
            a = a * Double(i)
        }
        return a
    }
    
    
}
