import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var display: UILabel!
    var userIsInMiddleOfTyping = false
    
    //if user never printed anything to display
    //then it is 0
    //else there should be input
    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle! //take title from sender
        print("Touched \(digit) digit!")
        
        if userIsInMiddleOfTyping{
            if let displayText = display.text{
                display.text = displayText + digit
            } else{
                display.text = digit
            }
        } else { //
            display.text = digit
            userIsInMiddleOfTyping = true
        }
    }
    
    //computed property
    var displayValue : Double{
        get{
            return Double(display.text!)!
        }
        set{
            display.text = String(newValue)
        }
    }

    private var brain = CalculatorBrain()
    
    @IBAction func performOperation(_ sender: UIButton) {
        if userIsInMiddleOfTyping{
            brain.setOperand(displayValue)
        }
        
        if let mathSymbol = sender.currentTitle{
            brain.performOperation(mathSymbol)
        }
        if let result = brain.result{
            displayValue = result
        }
    }
    
    
    

}

