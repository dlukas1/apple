import UIKit

func expandedForm(_ num: Int) -> String {
    // Do something
    let myString = String(num)
    var n = myString.count-1
    var result = ""
    for s in myString {
        if s != "0"{
            result += "\(s)\(String(repeating: "0", count: n)) + "
        }
n -= 1
    }
    return String(result.dropLast().dropLast())
}

print(expandedForm(42))

/*func expandedForm(_ num: Int) -> String {
    let digits = String(num).characters
    let maxZeros = digits.count - 1
    
    let parts = digits
        .enumerated()
        .filter { $0.element != "0" }
        .map { String($0.element) + String(repeating: "0", count: maxZeros - $0.offset) }
    
    return parts.joined(separator: " + ")
}
*/
