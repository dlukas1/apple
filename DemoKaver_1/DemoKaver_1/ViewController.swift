//
//  ViewController.swift
//  DemoKaver_1
//
//  Created by Dmitry Lukas on 26/09/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var helloLabel: UILabel!
    @IBAction func buttonClicked(_ sender: UIButton) {
        print("Button clicked: \(sender.currentTitle)")
        helloLabel.text = "Hi there, i'm Clicked!"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

