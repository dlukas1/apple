import UIKit


protocol PoliceCodes{
    
    //anyone who follows this protocol should implement it's methods
    func nineOhTwo()
    
}

class Officer{
    
    // it is delegate!!!
    var radio : PoliceCodes?
    
    func callItIn(){
        radio?.nineOhTwo()
    }
}

class Dispatcher : PoliceCodes{
    
    func nineOhTwo() {
        print("902 RECEIVED and HANDLED")
    }
    
    
}

let o = Officer()
let d = Dispatcher()

o.radio = d
o.callItIn()
