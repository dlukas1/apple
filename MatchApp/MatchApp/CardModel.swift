//
//  CardModel.swift
//  MatchApp
//
//  Created by Dmitry Lukas on 29/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import Foundation

class CardModel  {
    
    func getCards() -> [Card]{
        //Declare an array to store generated cards
        var generatedCardsArray = [Card]()
        //Randomly generate crds
        for _ in 1...8 {
            let randomNumber = arc4random_uniform(13)+1 //(0 to 12) + 1
            //TODO: Make it to generate only unique pairs of cards
            
            //log the number generated
            print(randomNumber)
            
            let cardOne = Card()
            cardOne.imageName = "card\(randomNumber)"
            generatedCardsArray.append(cardOne)
            
            let cardTwo = Card()
            cardTwo.imageName = "card\(randomNumber)"
            generatedCardsArray.append(cardTwo)
        }
        //TODO: Randomize array
        //Return array
        return generatedCardsArray
    }
}
