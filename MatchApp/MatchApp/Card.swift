
//
//  Card.swift
//  MatchApp
//
//  Created by Dmitry Lukas on 29/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import Foundation

class Card {
    
    var imageName = ""
    var isFlipped = false
    var isMatched = false
    
}
