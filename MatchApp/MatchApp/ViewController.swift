//
//  ViewController.swift
//  MatchApp
//
//  Created by Dmitry Lukas on 29/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var timerLabel: UILabel!
    
    var model = CardModel()
    var cardArray = [Card]()
    
    var firstFlippedCardIndex : IndexPath?
    
    var timer : Timer?
    var milliseconds : Float = 10 * 1000 // 10 seconds
    
    @IBOutlet weak var collectionView: UICollectionView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Call the getCards()
        cardArray = model.getCards()
        
        //Set ViewController as Delegate to a Collection View
        //When CollectionView needs to notify another class - it will use delegate
        collectionView.delegate = self
        
        //When CollectionView needs data it will use dataSource to communicate with ViewController
        collectionView.dataSource = self
        
        //Create timer
        timer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(timerElapsed), userInfo: nil, repeats: true)
        RunLoop.main.add(timer!, forMode: .common)
    }
    
    //MARK: - Timer Methods
    @objc func timerElapsed(){
        //reduce counter every time method is called
        milliseconds -= 1
        
        //convert to seconds
        let seconds = String(format: "%.2f", milliseconds/1000)
        
        //set label
        timerLabel.text = "Time Remaining: \(seconds)"
        
        //When timer is 0 stop it
        if milliseconds <= 0 {
            timer?.invalidate()
            timerLabel.textColor = UIColor.red
            
            //Check if there are any cards unmatched
            checkGameEnded()
        }
    }

    //MARK: -UICollectionView Protocol Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cardArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Get a CardCollectionViewCell object
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath )as! CardCollectionViewCell
        
        //get card that collectionView is trying to display
        let card = cardArray[indexPath.row]
        
        //set card to cell
        cell.setCard(card)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //check if there is any time left
        if milliseconds <= 0 {
            return
        }
        
        //get cell that user selected
        let cell =  collectionView.cellForItem(at: indexPath) as! CardCollectionViewCell
        
        //get card that user selected
        let card = cardArray[indexPath.row]
        
        if !card.isFlipped && !card.isMatched {
            //flip card
            cell.flip()
            
            //determine is it first card flipped
            if firstFlippedCardIndex == nil{
                firstFlippedCardIndex = indexPath
            } else {
                //then it is second card flipped
                //TODO: check matching
                checkForMatches(indexPath)
            }
        }
    }
    
    
    //MARK: - Game Logic Methods
    func checkForMatches(_ secondFlippedCardIndex : IndexPath){
        //get cells for 2 cards were revealed
        let cardOneCell = collectionView.cellForItem(at: firstFlippedCardIndex!) as? CardCollectionViewCell
        
        let cardTwoCell = collectionView.cellForItem(at: secondFlippedCardIndex) as? CardCollectionViewCell
        
        let cardOne = cardArray[firstFlippedCardIndex!.row]
        let cardTwo = cardArray[secondFlippedCardIndex.row]
        
        //compare two cards
        if cardOne.imageName == cardTwo.imageName{
            //its match
            //set statuses
            cardOne.isMatched = true
            cardTwo.isMatched = true
            
            //remove  cards from the grid
            cardOneCell?.remove()
            cardTwoCell?.remove()
            
            //check if there are any cards unmatched
            checkGameEnded()
        }
        else {
            //not match
            cardOne.isFlipped = false
            cardTwo.isFlipped = false
            cardOneCell?.flipBack()
            cardTwoCell?.flipBack()
        }
        //tell collectionView to reload cell of first card if it is nil
        if cardOneCell == nil{
            collectionView.reloadItems(at: [firstFlippedCardIndex!])
        }
        
        //reset property that tracks first card flipped
        firstFlippedCardIndex = nil
    }
    
    func checkGameEnded(){
        //determine if there are any cards unmatched
        var isWon = true
        
        for card in cardArray{
            if !card.isMatched {
                isWon = false
                break
            }
        }
        //messaging variables
        var title = ""
        var message = ""
        
        // if no then user won, stop game
        if isWon {
            if milliseconds > 0{
                timer?.invalidate()
            }
            title = "Congratulations!"
            message = "You've won!"
        }
        else {
            if milliseconds > 0 {
                return
            }
            else {
                title = "Game Over"
                message = "You've lost"
            }
        }
        showAlert(title, message)
    }
    
    func showAlert(_ title : String, _ message : String){
        //show won/loss message
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }

}

