//
//  CardCollectionViewCell.swift
//  MatchApp
//
//  Created by Dmitry Lukas on 29/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var frontImageView: UIImageView!
    
    @IBOutlet weak var backImageView: UIImageView!
    
    var card : Card?
    
    func setCard(_ card:Card){
        //keep track of card that gets passed in
        self.card = card
        
        if card.isMatched {
            //if card has been matched make it invisible
            backImageView.alpha = 0
            frontImageView.alpha = 0
            return
        }
        else {
            //if was not matched make visible
            backImageView.alpha = 1
            frontImageView.alpha = 1
        }
        
        frontImageView.image = UIImage(named: card.imageName)
        
        //Determine is card is flip up or down
        if card.isFlipped{
            //make sure front image is on top
            UIView.transition(from: backImageView, to: frontImageView, duration: 0, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
            
        } else {
            //make sure back image is on top
            UIView.transition(from: frontImageView, to: backImageView, duration: 0, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        }
    }
    
    func flip(){
        //change images
        UIView.transition(from: backImageView, to: frontImageView, duration: 0.3, options: [.transitionFlipFromLeft, .showHideTransitionViews], completion: nil)
        card?.isFlipped = true
    }
    
    func flipBack(){
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            
            UIView.transition(from: self.frontImageView, to: self.backImageView, duration: 0.3, options: [.transitionFlipFromRight, .showHideTransitionViews], completion: nil)
            
        }
        card?.isFlipped = false
    }
    
    //removes both image views
    func remove(){
        
            UIView.animate(withDuration: 0.3, delay: 0.5, options: .curveEaseOut, animations: {
                self.frontImageView.alpha = 0
            }, completion: nil)
            
        
        
        
    }
    
}
