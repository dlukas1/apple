import UIKit

//Write a function called repeatStr which repeats the given string string exactly n times.

/*
func repeatStr(_ n: Int, _ string: String) -> String {
    return String(repeating: string, count: n)
}*/
func repeatStr(_ n: Int, _ string: String) -> String {
    var str = ""
    var nn = n
    repeat {
        str += string
        nn -= 1
    }
        while nn>0
    
    
    
    return str
}
