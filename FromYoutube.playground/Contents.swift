import UIKit

class SpaceShip {
    
    //properties of class
    var fuelLevel = 20
    
    func cruise(){
        //initialize cruizing
        
    }
    
    func thrust(){
        //start rocket thrusters
        if fuelLevel > 10 {
            print("Let's fly")
            fuelLevel -= 10
        } else {
            print("Low fuel")
        }
    }
}

let myShip = SpaceShip()
print(myShip.fuelLevel)
myShip.thrust()
myShip.thrust()







/*
func sayHi(name:String){
    print("hi there, \(name)")
}

func sayHiNoLabel(_ name:String){
    print("Hi with no label, \(name)")
}

sayHi(name: "Dmitri")
sayHiNoLabel("Billy")


//func with return
func addFour(_ x:Int) -> Int{
    return x+4
}

var mySum = addFour(10)
print(mySum)
*/
