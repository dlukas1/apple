import UIKit

func printPerson(name: String, age:Int){
    let personInfo = (name, age)
    switch personInfo{
    case("Bob", 33):
        print("Hello Bob my frend for 33 years")
    case (_, 1...18):
        print("Hello my young friend \(personInfo.0) of \(personInfo.1) ages old")
        
    case (_, _):
        print("Hello \(personInfo.0) age is \(personInfo.1)")
    }
}


printPerson(name: "Bob", age: 33)

printPerson(name: "Bill", age: 22)

printPerson(name: "Billy Jr", age: 3)
