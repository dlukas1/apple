import UIKit

enum PrinterError : Error{
    case outOfPaper
    case noToner
    case onFire
}

func send(job:Int, toPrinter printerName: String) throws -> String{
    if printerName == "Never Has Toner"{
        throw PrinterError.noToner
    }
    return "Job Sent!"
}

do{
    let printerResponse = try send(job: 404, toPrinter: "HpDeskMonster")
        print(printerResponse)
} catch{
    print(error)
}


var fridgeIsOpen = false
let frigdeContent = ["Milk", "Eggs", "Beer"]

func fridgeContains(_ food: String) -> Bool{
    fridgeIsOpen = true
    defer {
        fridgeIsOpen = false
    }
    return frigdeContent.contains(food)
}
print(fridgeContains("Apple"))
