import UIKit

var numbers2 = [3,14,15,2,33,84,362,5,4,8,76,12,43]
let mappedNUmbers = numbers2.map({number in 2 * number})
print(mappedNUmbers)
let sortedNumbers = mappedNUmbers.sorted{$0 < $1}
print(sortedNumbers)
//Функция может принимать другую функцию в качестве аргумента.
/*
func hasAnyMatches(list: [Int], condition: (Int) -> Bool) -> [Int] {
    var arr = [Int]()
    for item in list {
        if condition(item) {
            arr.append(item)
        }
    }
    return arr
}
func lessThanTen(number: Int) -> Bool {
    return number > 13
}
 */
var numbers = [20, 19, 7, 12]
//print(hasAnyMatches(list: numbers, condition: lessThanTen))
numbers = numbers.map({ (number: Int) -> Int in
    //let result = 3 * number
    //Перепишите замыкание так, чтобы оно вернуло ноль для всех нечетных чисел.
    let result = (number % 2 == 0) ? number : 0
    return result
})
print(numbers)

//Функции - это объект первого класса. Это означает, что результатом функции может быть другая функция.
/*
func getIncrement()->((Int) -> Int){
    func addTen(to num:Int)->Int{
        return num + 10
    }
return addTen
}
var inc = getIncrement()
print(inc(5))
*/

/*
func return16() -> Int{
    var a = 10
    func add(){
        a += 5
    }
    add()
    return a
}
print(return16())
 */
/*
func getStatistic(of scores : [Int]) -> (min:Int, max:Int, avg:Int){
    var min = scores[0]
    var max = scores[0]
    var avg = scores[0]
    var sum = 0
    
    for score in scores{
        if score > max{
            max = score
        } else if score < min{
            min = score
        }
        sum += score
    }
    avg = sum / scores.count
    return(min, max, avg)
}
let stats = getStatistic(of: [1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2])
print("Min is \(stats.min), max is \(stats.1) and avg is \(stats.avg)")
*/


/*
func greet(to name : String, at day: String) -> String{
    return "Hello \(name) at happy \(day)"
}
print(greet(to: "Ahmed", at: "Monday"))
*/



/*
let tea = "tea of island long"

switch tea {
case "black":
    print("Add some sugar !")
case "green", "white":
    print("Good for health")
case let x where x.hasSuffix("long"):
    ("Don't go to work tomorrow!")
default:
    print("I love tea!")
}
*/
