//
//  ViewController.swift
//  CardMemoryGame
//
//  Created by Dmitry Lukas on 12/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Refresh label when new value is set
    var fipCount = 0 {
        didSet{
            labelFlipsCount.text = "Flips: \(fipCount)"
        }
    }

    @IBOutlet weak var labelFlipsCount: UILabel!
    
    //CardList is collection of our cards with indexes
    @IBOutlet var cardList: [UIButton]!
    
    var emojiChoices = ["👻","🐝","🎃","💩"]
    
    @IBAction func cardClicked(_ sender: UIButton) {
        
        //cardNumber is index of card clicked from collection
        if let cardNumber = cardList.index(of: sender){
            //Select emoji with same number as card index
            let emoji = emojiChoices[cardNumber]
            flipCard(withEmoji: emoji, on: sender)
        } else{
            print("Unknown card clicked")
        }
    }
    
    func flipCard(withEmoji emoji : String, on button : UIButton){
        //if currently emoji is open increase counter and replace with X
        if button.currentTitle == emoji{
            fipCount += 1
            button.setTitle("X", for: UIControl.State.normal)
           
        }
        else{
            //if card is with back side X - show emoji
            button.setTitle(emoji, for: UIControl.State.normal)
        }
    }
    
    
}
