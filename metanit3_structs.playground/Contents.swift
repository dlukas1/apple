import UIKit


enum ServerResponse{
    case result(String, String)
    case failure(String)
    case test(String )
}
let success = ServerResponse.result("06.00 am", "22.20 pm")
let failure = ServerResponse.failure("Out of memory")


switch success {
case let .result(sunrise, sunset):
    print("Sunrise at \(sunrise) and sunset at \(sunset)")
case let .failure (message):
    print("Failure ... \(message)")

case .test(_):
    print("Testing with test message")
}


enum Suit{
    case spades, hearts, diamonds, clubs
    func simpleDesc() -> String{
        switch self {
        case .spades:
            return "spades"
        case .hearts:
            return "hearts"
        case .diamonds:
            return "diamonds"
        case .clubs:
            return "clubs"
        }
    }
    func color() -> String{
        switch self {
        case .spades, .clubs:
            return "black"
        case .hearts, .diamonds:
            return "red"
        }
    }
}


enum Rank : Int{
    case ace = 1
    case two, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king
    
    func simpleDescription() -> String {
        switch self {
        case .ace:
            return "ace"
        case .jack:
            return "jack"
        case .queen:
            return "queen"
        case .king:
            return "king"
        default:
            return String(self.rawValue)
        }
    }
    
    func compareRawValues(_ cardA: Rank, _ cardB : Rank) -> Rank{
        return (cardA.rawValue>cardB.rawValue) ? cardA : cardB
    }
}
/*
let ace = Rank.ace
let king = Rank.king
print(ace.compareRawValues(ace, king))
*/
struct Card{
    var rank : Rank
    var suit : Suit
    func simpleDesc() -> String{
        return "The \(rank.simpleDescription()) of \(suit.simpleDesc())"
    }
}

let kingOfClubs = Card(rank: .king, suit: .clubs)
print(kingOfClubs.simpleDesc())
