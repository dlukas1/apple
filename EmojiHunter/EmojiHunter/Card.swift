//
//  Card.swift
//  EmojiHunter
//
//  Created by Dmitry Lukas on 26/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import Foundation

struct Card {
    
    var isFaceUp = false
    var identifier: Int
    var isMatched = false //if true - this card is out of game
    
    static var IdentityFactory = 0
    
    init() {
        identifier = Card.getUniqueCardId()
    }
    
    static func getUniqueCardId() -> Int{
        Card.IdentityFactory += 1
        return Card.IdentityFactory
    }
}
