//
//  ViewController.swift
//  EmojiHunter
//
//  Created by Dmitry Lukas on 17/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //lazy - var is initialized on first accsess, init is postponed
    lazy var game = Memory(numberOfPairsOfCards: (cardList.count+1) / 2)
    
    @IBOutlet var cardList: [UIButton]!
    //variable to count how many cards are currently open
    var openedCards = 0
    
    @IBAction func cardClicked(_ sender: UIButton) {
        //cardNumber is index of card clicked from collection
        if let cardNumber = cardList.index(of: sender){
            
            game.chooseCard(at: cardNumber) //flip card
            updateViewFromModel()
            fipCount += 1
        }
        else {
            print("Unknown card clicked")
        }
    }
    @IBOutlet weak var flipCountLabel: UILabel!
    //Refresh label when new value is set
    var fipCount = 0 {
        didSet{
            flipCountLabel.text = "Flips: \(fipCount)"
        }
    }
    var emojiList = ["🎃","👻","🤡","💩","☠️","😻","👹","🤖","👺","🐝","🐳","🍔"]//.shuffled()
    let secretCard = "X"
    
    func updateViewFromModel(){
        for index in cardList.indices {
            let button = cardList[index]
            let card = game.cards[index]
            
            if (card.isFaceUp){
                button.setTitle(emojiForCard(for: card), for: UIControl.State.normal)
            } else {
                
                //button.backgroundColor = card.isMatched?
                if (card.isMatched){
                    button.setTitle("", for: UIControl.State.normal)
                    button.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                } else {
                    button.setTitle(secretCard, for: UIControl.State.normal)
                }
            }
        }
    }
    
    //var emoji = Dictionary<Int, String>()
    var emoji = [Int: String]()
    func emojiForCard(for card: Card) -> String{
        if emoji[card.identifier] == nil && emojiList.count>0 {
            let randomIndex = Int(arc4random_uniform(UInt32 (emojiList.count)))
            emoji[card.identifier] = emojiList.remove(at: randomIndex)
        }
            return emoji[card.identifier] ?? "?"
    }
}

