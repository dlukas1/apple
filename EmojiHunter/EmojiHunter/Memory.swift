//
//  Memory.swift
//  EmojiHunter
//
//  Created by Dmitry Lukas on 26/10/2018.
//  Copyright © 2018 Dmitry Lukas. All rights reserved.
//

import Foundation

class Memory {
    
    var cards = [Card]()    //empty array undefined length
    
    var faceUpCardIndex: Int? //optional
    
    
    func chooseCard(at index:Int){
        
        if cards[index].isMatched {
            return
        }
        if let matchIndex = faceUpCardIndex, matchIndex != index {
            //are they same
            if cards[matchIndex].identifier == cards[index].identifier{
                cards[matchIndex].isMatched = true
                cards[index].isMatched = true
            }
            cards[index].isFaceUp = true
            faceUpCardIndex = index
        } else {
            //no cards are up or 2 cards are up
            for flipDownIndex in cards.indices{
                cards[flipDownIndex].isFaceUp = false
            }
            cards[index].isFaceUp = true
            faceUpCardIndex = nil
        }
        
        //flip card
        //cards[index].isFaceUp = !cards[index].isFaceUp
    }
    
    //Constructor
    init(numberOfPairsOfCards:Int){
        for _ in 0..<numberOfPairsOfCards{ // ..< repeat while value is smaller than limit
            let card = Card()
            //let matchingCard = card //create a copy
            //Add cards to array
            //cards.append(card)
            //cards.append(matchingCard)
            
            //add two same cards to collection
            cards += [card, card]
        }
        cards.shuffle()
    }
    
}
