import UIKit

class namedShape{
    var numberOfSides = 0
    let dimentions = 3
    var name:String
    
    init(_ mName: String){
        self.name = mName
    }
    
    func simpleDesc() -> String{
        return "A shape with \(numberOfSides) sides"
    }
    func getDimentions() -> String{
        return "I have \(dimentions) dimentions"
    }
}

class Square : namedShape{
    var sideLength: Double
    
    init(sideL: Double, name: String) {
        self.sideLength = sideL
        super.init(name)
        numberOfSides = 4
    }
    func area() -> Double{
        return sideLength*sideLength
    }
    override func simpleDesc() -> String {
        return "A square with \(numberOfSides) sides"
    }
}




class Circle : namedShape{
    var radius : Double
    
    init(rad : Double, name : String){
        self.radius = rad
        super.init(name)
    }
    func area() -> Double{
        return 3.14 * radius*radius
    }
    
    override func simpleDesc() -> String {
        return "I am a circle with radius \(radius) and area \(area())"
    }
}

class EquivalentTriangle : namedShape{
    var sideLength : Double
    
    init(sideLengt : Double, name : String ){
        self.sideLength = sideLengt
        super.init(name)
        numberOfSides = 3
    }
    var perimeter: Double{
        get{
            return sideLength * 3
        }
        set{
            sideLength = newValue / 3
        }
    }
    override func simpleDesc() -> String {
        return "An triangle with \(numberOfSides) sides each \(sideLength) perimeter \(perimeter)"
    }
}

var trig = EquivalentTriangle(sideLengt: 10.0, name: "myTrig")
print(trig.perimeter)
print(trig.simpleDesc())
