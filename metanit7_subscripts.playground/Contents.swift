import UIKit


class Book{
    var name : String
    init(name : String) {
        self.name = name
    }
}

class Library{
    var books:[Book] = [Book]()
    
    init() {
        books.append(Book(name: "Ender's Game"))
        books.append(Book(name: "Harry Potter"))
    }
    
    subscript(index: Int) -> Book{
        
        get{
            return books[index]
        }
        set(newValue){
            books[index] = newValue
        }
    }
}


var myLibr = Library()
var firstBook = myLibr[0]
print(firstBook.name)

myLibr.books.append(Book(name: "Treasure Island"))
for book in myLibr.books{
    print(book.name)
}
